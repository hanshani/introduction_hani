| **Head** | **Information** |
| ------ | ----------- |
| Logbook entry number | 05 |
| Name   | Hani Abdullanahadi |
| Matrix number | 195456 |
| Year   | 4 (final year) |
| Subsystem   | Propulsion and Power |
| Group | 6 |
| Date   | 22/11/2021-26/11/2021 |


| **Target** | **Details** |
| ------ | ----------- |
| Agenda   | <br>- Tested second motor with power supply (16V) and also battery (15.8V) with ESC Number 4. <br>- Error in troubleshooting current problem due to poor connection.  <br>- Prepared an excel sheet to calculate thrust, power, and maximum power avalable. | 
| Goals | <br>- To get the actual thrust values from velocity at 0-10 m/s. <br>- To get the actual power values from velocity at 0-10 m/s. <br>- To get the actual maximum power avalible value in order to know what is the maximum power we can take. <br>- We also removed the outliers from the previous graph for the old motor data.  |
| Decisions | <br>- The supply to the motor and the size of the propeller had to be decided upon. <br>- Decided the member who would go for the lab test.|
| Method   | <br>- Power supply and battery supply are characteristics of what is used in this section. <br>- Voluntary participation was used to choose which team members would be sent to the lab for testing thrust. |
| Impact   | <br>-  Propeller misalignment resulted in excessive vibration and unsteady thrust. <br>- We were unable to diagnose the problem because of a lost connection at the ESC. |
| Next step | <br>- Observe how the new motor performs.  <br>- Online meeting with Dr.Ezanee on Wednesday to wrap up things that are needed. |
