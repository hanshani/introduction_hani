| **Head** | **Information** |
| ------ | ----------- |
| Logbook entry number | 02 |
| Name   | Hani Abdullanahadi |
| Matrix number | 195456 |
| Year   | 4 (final year) |
| Subsystem   | Propulsion and Power |
| Group | 2 |
| Date   | 01/11/2021-05/11/2021 |


| **Target** | **Details** |
| ------ | ----------- |
| Agenda   | <br>- Tested the old motor that we have. <br>- Determined the amount of current used when the motor is working at a maximum thrust.| 
| Goals | <br>- To get the right propeller and amount of curretn suits with motor that’s working at maximum thrust.<br>- Confirmed an appointment with Dr.Ezanee in order to use the lab H2.3 to perform thrust test. |
| Decisions | <br>-We decided to use 16 and 24 inches of propeller to be tested.<br>- Decided the member who would go for the lab test.|
| Method   | <br>- We had used the software called RCbenchmark to test out the motor’s thrust. |
| Impact   | <br>-  24 inches propeller produced 2kg of thrust but the motor stopped working and one point and burned later after that, the reason was it might had reached the maximum capacity. |
| Next step | <br>- Test out dr. Ezanee’a motor and do the trolley test with dr. Ezanee and join his extra class. Also, we will be doing the calibration of the test system. |

