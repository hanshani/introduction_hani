| **Head** | **Information** |
| ------ | ----------- |
| Logbook entry number | 03 |
| Name   | Hani Abdullanahadi |
| Matrix number | 195456 |
| Year   | 4 (final year) |
| Subsystem   | Propulsion and Power |
| Group | 2 |
| Date   | 08/11/2021-12/11/2021 |


| **Target** | **Details** |
| ------ | ----------- |
| Agenda   | <br>- Preparation and calibration of the test system. <br>- We enrolled in Dr. Ezanee's additional class after receiving some advice from him. <br>- Dr. Ezanee's motor was put through its tests. <br>- We put the airship through its test on the trolley test. | 
| Goals | <br>- In order to obtain the proper size propeller suit for the project. <br>- We were able to get some preliminary estimates of the number of thrusts that would be required to propel the airship. <br>- Dr. Ezanee provided us with some further propulsion expertise.  |
| Decisions | <br>- Determined the most efficient technique to conduct the trolley test in order to prevent errors.<br>- Decided the member who would go for the lab test.|
| Method   | <br>- Estimated the required force with the use of a trolley and a spring balance. <br>- Voluntary participation was used to choose which team members would be sent to the lab for testing thrust. |
| Impact   | <br>-  When conducting the test, caution is advised to ensure accuracy and that the test is performed on a smooth surface in order to decrease friction between the trolley and the ground. |
| Next step | <br>- Keep on putting the airship through its tests with the trolley. <br>- If at all feasible, put the new motor through its tests as well. |


