| **Head** | **Information** |
| ------ | ----------- |
| Logbook entry number | 06 |
| Name   | Hani Abdullanahadi |
| Matrix number | 195456 |
| Year   | 4 (final year) |
| Subsystem   | Propulsion and Power |
| Group | 6 |
| Date   | 29/11/2021-03/12/2021 |


| **Target** | **Details** |
| ------ | ----------- |
| Agenda   | <br>- Online discussion on landing options for the airship, contribution of vibration, and purpose of current relative to thrust graph plotting (important as these are quantitive numerical results of proof we have data)  with Dr.Ezanee. | 
| Goals | <br>- To develop a landing mechanism for the airship and present it to the community. <br>- To come out a method to suppress vibration as much as possible as it does affect the overall performance of the airship. <br>- To obtain the range of operational current and maximum continuous current. <br>- Analysed effect on battery capacity (Ah - amp hours) and how it affects endurance and range. <br>- how long we would expect the battery to work when it works on a constant 4m/s cruise speed. <br>- Current VS Thrust graph was generated.   |
| Decisions | <br>- Dr.Ezanee presented us with three alternatives on which we could express our opinions. <br>- Each option has its own set of advantages and disadvantages.|
| Method   | <br>- Through the Zoom platform, we had an online meeting. |
| Impact   | <br>-  To make up for the fact that a few members could not make it, we recorded the meeting and played them again for those who couldn't make it. <br>- Each landing method has its own advantages and disadvantages, which requires careful consideration. <br>- Vibration is still the main issue here. |
| Next step | <br>- Take one week break during mid-semester and then come back with fresh mind. |
