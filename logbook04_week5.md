| **Head** | **Information** |
| ------ | ----------- |
| Logbook entry number | 04 |
| Name   | Hani Abdullanahadi |
| Matrix number | 195456 |
| Year   | 4 (final year) |
| Subsystem   | Propulsion and Power |
| Group | 6 |
| Date   | 15/11/2021-19/11/2021 |


| **Target** | **Details** |
| ------ | ----------- |
| Agenda   | <br>- Run thrust test using 4 old motors. Solved 48 steps design problems <br>- Notified what parameters/numbers that is needed to find for Subsystems and how are we planning to find them. <br>- Learned some theoretical part from Dr Ezanee. | 
| Goals | <br>- To get the thrust from diffent kind of motors (4 motors) to be able to drive the airship. <br>- We were able to get some parameters/numbers that is needed for now. <br>- To get the appropriate size propeller suit for the project.  |
| Decisions | <br>- Determined the maximum thrust and current values from each 4 motors .<br>- Made sure that motors are in good condition. <br>- All motors were tested using 17 inches propeller. <br>- Decided the member who would go for the lab test.|
| Method   | <br>- Used the spreadsheet to come out with the grapgh of:   <br> 1. Current over thrust. <br> 2. Thrust over time. <br> 3. Current over thrust. |
| Impact   | <br>-  When conducting the test, we resulted that only motor 2 and 3 are in good condition. Although, motor 3 still has high current. <br>- Motor 1 resulted that it could not spin properly. <br>- Motor 4 is burned, the reason is that the propeller was probably bigger than it could tolerate. |
| Next step | <br>- Calculate the thrust/drag from Dr Ezanee's idea.  <br>- Go to lab on Wednesday to test motor with battery. |

